
package psi.graphics;

import javax.swing.RepaintManager;
import javax.swing.JComponent;

/*
Klasa NullRepaintManager dziedziczy po RepaintManager i nie wykonuje żadnych operacji 
Jest przdatna w przypadku aplikacji samodzielnie obsługujących renderowanie.
*/

public class NullRepaintManager extends RepaintManager {

    /*
    Instaluje NullRepaintManager
    */
    public static void install()
    {
        RepaintManager repaintManager=new NullRepaintManager();
        repaintManager.setDoubleBufferingEnabled(false);
        RepaintManager.setCurrentManager(repaintManager);
    }
    
    public void addInvalidComponent(JComponent c)
    {
        //Nic nie rób
    }
    
    public void addDirtyRegion(JComponent c, int x, int y, int w, int h)
    {
        //Nie rób nic
    }
    
    public void markCompletelyDirty(JComponent c)
    {
        //nic nie rób
    }
    
    public void paintDirtyRegions()
    {
        //Nie rób nic
    }
}
