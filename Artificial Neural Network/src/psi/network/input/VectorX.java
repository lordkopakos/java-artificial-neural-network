package psi.network.input;

//Klasa VectorX przedstawia wektor informacji wejsciowych od x1 do x(m_iLength)
public class VectorX {

    public enum Learn{
        WITH_TEACHER,
        WITHOUT_TEACHER
    }
    private Learn learnMethod;
    
    private double [] m_fCoordinates;
    private int m_iLength;
    
    private String tag;
    
    private double z;
    
    //Konstruktory klasy VectorX
    public VectorX(int length, Learn learnMethod){
        this.learnMethod=learnMethod;
        m_iLength=length;
        m_fCoordinates=new double[length];
    }
    
    public VectorX(int length){
        this(length, Learn.WITH_TEACHER);
    }
    
    
    public void VectorX(VectorX v){
        if(this.m_iLength==v.m_iLength){
            this.m_fCoordinates=v.m_fCoordinates;
            this.m_iLength=v.m_iLength;
            setTag("Empty");
        }
        else{
            System.out.println("Wektory maja rozna dlugosc");
        }
    }
    
    //Operacje matematyczne na wektorach
    public void add(VectorX v){
        if(this.m_iLength==v.m_iLength){
            for(int i=0; i<this.m_iLength; i++){
                this.m_fCoordinates[i]+=v.m_fCoordinates[i];
            }
            this.z+=v.z;
        }
        else{
            System.out.println("Wektory maja rozna dlugosc");
        }
    }
    public void sub(VectorX v){
        if(this.m_iLength==v.m_iLength){
            for(int i=0; i<this.m_iLength; i++){
                this.m_fCoordinates[i]-=v.m_fCoordinates[i];
            }
            this.z-=v.z;
        }
        else{
            System.out.println("Wektory maja rozna dlugosc");
        }
    }
    
    //Pobiera wartość współrzędnej wektora
    public double getCoordinates(int index){
        return this.m_fCoordinates[index];
    }
    
    //Ustawia wartość współrzędnej wektora
    public void setCoordinates(int index, double value){
        this.m_fCoordinates[index]=value;
    }
    public double getZ(){
        return this.z;
    }
    
    //Ustawia wartość współrzędnej wektora
    public void setZ(double value){
        this.z=value;
    }
    
    public int getLength(){
        return this.m_iLength;
    }
    public void setLength(int length){
        this.m_iLength=length;
    }
    
    public String getTag(){
        return this.tag;
    }
    public void setTag(String tag){
        this.tag=tag;
    }
    
    public Learn getLearnState(){
        return this.learnMethod;
    }
    public void setLearnState(Learn learnMethod){
        this.learnMethod=learnMethod;
    }
    
    @Override
    public String toString(){
        String result = getTag()+" ";
        for(int i=0; i<m_iLength;i++){
            result+="x"+i+"="+m_fCoordinates[i]+"\t";
        }
        if(learnMethod==Learn.WITH_TEACHER)
            result=result+"z="+getZ();
        else
            result=result+"\t WITHOUT TEACHER";
        return result;
    }
    
}
