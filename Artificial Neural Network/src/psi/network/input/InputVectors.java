/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.network.input;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 *
 * @author lordkopakos
 */
public class InputVectors {
    
    private String filepath;
    private ArrayList<VectorX> learningSet;
    private int vectorLength;

    public InputVectors() {
        filepath = "learn/trainingSet/";
    }
    
    
    public void initLearningSet(String fileName) throws IOException {

        ArrayList lines = new ArrayList();

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        int heightLearningSet = 0;
        heightLearningSet = lines.size();
        learningSet = new ArrayList<>(heightLearningSet);

        for (int y = 0; y < heightLearningSet; y++) {

            String line = (String) lines.get(y);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);
            int widthLearningSet = xn.length;
            VectorX v = new VectorX(widthLearningSet);
            v.setLearnState(VectorX.Learn.WITHOUT_TEACHER);

            v.setTag(xn[0]);

            vectorLength = widthLearningSet-1;

            for (int i = 1; i < widthLearningSet; i++) {
                double value = Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            boolean add = learningSet.add(v);
            if (add) {
                System.out.println("Nowy VectorX został dodany");
                System.out.println(v);
            }
        }
    }

    public VectorX getVectorX(int index) {
        return learningSet.get(index);
    }
    
    public int getVectorLength() {
        return vectorLength;
    }
    
    public String getFilepath() {
        return filepath;
    }

    public ArrayList<VectorX> getLearningSet() {
        return learningSet;
    }
    
    

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }
}
