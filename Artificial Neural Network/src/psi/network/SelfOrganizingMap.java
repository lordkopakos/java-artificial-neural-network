/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.network;

import psi.network.input.VectorX;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import psi.networkTest.NeuralNetwork;
import psi.neural.LearningCell;

/**
 *
 * @author lordkopakos
 */
public class SelfOrganizingMap {

    private LearningCell[][] neurals;
    private int width;
    private int height;

    private int winnerX;
    private int winnerY;

    private final String filepath;
    private ArrayList lines;
    private ArrayList<VectorX> learningSet;

    private int input;

    public SelfOrganizingMap(int width, int height) throws IOException {
        this.width = width;
        this.height = height;
        neurals = new LearningCell[width][height];
        filepath = "learn/trainingSet/";
        initLearningSet("zoo.txt");
        normalizeAllVectorsX();
        initNeuralsMatrix();
        init();
    }

    private void initNeuralsMatrix() {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                LearningCell cell = new LearningCell();
                cell.addInput(input);
                cell.setRandomWeight(0, 0.5);
                //normalizeVectorX(learningSet.get(0));
                setVectorXAsInputDataForCell(0, cell);
                neurals[i][j] = cell;
            }
        }
    }

    public void winnerTakesAll() {
        int counter = 0;
        size=4;
        for (VectorX vectorX : learningSet) {
            //normalizeVectorX(vectorX);
            setVectorXAsInputDataForAllCells(counter);
            findWinner(vectorX);
            System.out.print(vectorX.getTag() + "\t");
            System.out.println("Znaleziony zwycięzca: neuron [" + winnerX + "] [" + winnerY + "] \n");
            //System.out.println("SPRAWDZENIE WEKTORA"+ vectorX);
            neurals[winnerX][winnerY].kohonenRule(vectorX, 1);
            graphx.drawPoint(winnerX, winnerY, new Color(winnerX / 20, winnerY / 20, winnerX * winnerY / 40));
            counter++;
        }
    }

    public void winnerTakesMost(int radiusNeighbors) {
        int counter = 0;
        size=4*radiusNeighbors+4;
        for (VectorX vectorX : learningSet) {
            //normalizeVectorX(vectorX);
            findWinner(vectorX);
            System.out.print(vectorX.getTag() + "\t");
            System.out.println("Znaleziony zwycięzca: neuron [" + winnerX + "] [" + winnerY + "] \n");
            neurals[winnerX][winnerY].kohonenRule(vectorX, 1);
            updateWinnerAndNeighbors(vectorX, radiusNeighbors, 0.5);
            graphx.drawPoint(winnerX, winnerY, new Color(winnerX / 20, winnerY / 20, winnerX * winnerY / 40));
            counter++;
        }
    }

    public void updateWinnerAndNeighbors(VectorX vectorX, int radiusNeighbors, double powerNeighbors) {
        int startX = winnerX - radiusNeighbors;
        int startY = winnerY - radiusNeighbors;

        int ix = 2 * radiusNeighbors + 1;
        int iy = 2 * radiusNeighbors + 1;
        

        for (int i = ix; i >= 0; i--) {
            for (int j = iy; j >= 0; j--) {
                if (startX < 0) {
                    ix += startX;
                    startX = 0;
                }
                if (startY < 0) {
                    iy += startY;
                    startY = 0;
                }
                if (startX < neurals.length) {
                    continue;
                }
                if (startY < neurals.length) {
                    continue;
                }
                if (startX == winnerX && startY == winnerY) {
                } else {
                    neurals[startX][startY].kohonenRule(vectorX, powerNeighbors);
                }
                startY++;
            }
            startY = startY - iy;
            startX++;
        }

    }

    private void normalizeAllVectorsX() {
        for (int i = 0; i < learningSet.size(); i++) {
            normalizeVectorX(i, learningSet.get(i));
        }
    }

    private void normalizeVectorX(int index, VectorX vectorX) {
        double vectorLength = countVectorXLength(vectorX);
        for (int i = 0; i < vectorX.getLength(); i++) {
            double currentCoordinatesX = vectorX.getCoordinates(i) / vectorLength;
            vectorX.setCoordinates(i, currentCoordinatesX);
        }
        learningSet.set(index, vectorX);
    }

    private double countVectorXLength(VectorX vectorX) {
        double vectorLength = 0;
        for (int i = 0; i < vectorX.getLength(); i++) {
            vectorLength += Math.pow(vectorX.getCoordinates(i), 2);
        }
        vectorLength = Math.sqrt(vectorLength);
        return vectorLength;
    }

    private void findWinner(VectorX vectorX) {
        //double valueCurrentWinner=neurals[0][0].getMembranePotential();
        double valueCurrentWinner = euclidesMeasure(neurals[0][0], vectorX);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                //double valueNewNeural=neurals[i][j].getMembranePotential();
                double valueNewNeural = euclidesMeasure(neurals[i][j], vectorX);
                if (valueNewNeural < valueCurrentWinner) {
                    valueCurrentWinner = valueNewNeural;
                    setWinnerCoordinates(i, j);
                }
            }
        }
    }

    private double euclidesMeasure(LearningCell cell, VectorX vectorX) {
        double result = 0;
        for (int i = 0; i < input; i++) {
            result += Math.pow(vectorX.getCoordinates(i) - cell.getInputWeight(i), 2);
        }
        return result = Math.sqrt(result);
    }

    private double manhattanMeasure(LearningCell cell, VectorX vectorX) {
        double result = 0;
        for (int i = 0; i < input; i++) {
            result += Math.abs(vectorX.getCoordinates(i) - cell.getInputWeight(i));
        }
        return result = Math.sqrt(result);
    }

    private double lInfinityMeasure(LearningCell cell, VectorX vectorX) {
        double result = 0;
        for (int i = 0; i < input; i++) {
            double newResult = Math.abs(vectorX.getCoordinates(i) - cell.getInputWeight(i));
            if (result < newResult) {
                result = newResult;
            }
        }
        return result;
    }

    private double maxMembranePotential(LearningCell cell) {
        return cell.getMembranePotential();
    }

    private void setWinnerCoordinates(int x, int y) {
        winnerX = x;
        winnerY = y;
    }

    private void setVectorXAsInputDataForAllCells(int index) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                setVectorXAsInputDataForCell(index, neurals[i][j]);
            }
        }
    }

    private void setVectorXAsInputDataForCell(int index, LearningCell cell) {
        for (int k = 0; k < input; k++) {
            cell.setInputData(k, learningSet.get(index).getCoordinates(k));
        }
    }

    public void initLearningSet(String fileName) throws IOException {

        lines = new ArrayList();

        // odczytanie wszystkich wierszy z pliku do listy
        BufferedReader reader = new BufferedReader(
                new FileReader(filepath + fileName));
        while (true) {
            String line = reader.readLine();
            // koniec wierszy do odczytywania
            if (line == null) {
                reader.close();
                break;
            }

            // dodawanie wszystkich wierszy poza komentarzami
            if (!line.startsWith("#")) {
                lines.add(line);
            }
        }

        int heightLearningSet = 0;
        heightLearningSet = lines.size();
        learningSet = new ArrayList<>(heightLearningSet);

        for (int y = 0; y < heightLearningSet; y++) {

            String line = (String) lines.get(y);

            Pattern comma = Pattern.compile(",");
            String[] xn = comma.split(line);
            int widthLearningSet = xn.length;
            VectorX v = new VectorX(widthLearningSet);
            v.setLearnState(VectorX.Learn.WITHOUT_TEACHER);

            v.setTag(xn[0]);

            input = widthLearningSet;

            for (int i = 1; i < widthLearningSet; i++) {
                double value = Double.valueOf(xn[i]);
                v.setCoordinates(i, value);
            }
            boolean add = learningSet.add(v);
            if (add) {
                System.out.println("Nowy VectorX został dodany");
                System.out.println(v);
            }
        }
    }

    JFrame frame;
    private static XYGraph graphx;
    private JLabel x_weight;
    private JLabel y_weight;
    private int size;

    public void init() {
        frame = new JFrame();
        frame.setBounds(100, 100, 599, 420);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        graphx = new XYGraph();
        graphx.setBorder(new LineBorder(new Color(0, 0, 0)));
        graphx.setBackground(Color.WHITE);
        graphx.setBounds(10, 11, 360, 360);
        frame.getContentPane().add(graphx);

        JButton btnAddPoint = new JButton("Next learning era");
        btnAddPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                winnerTakesAll();
            }
        });
        btnAddPoint.setBounds(408, 55, 144, 23);
        frame.getContentPane().add(btnAddPoint);

        JLabel lblWagaX = new JLabel("Waga X");
        lblWagaX.setHorizontalAlignment(SwingConstants.CENTER);
        lblWagaX.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblWagaX.setBounds(408, 89, 58, 23);
        frame.getContentPane().add(lblWagaX);

        JLabel lblWagaY = new JLabel("Waga Y");
        lblWagaY.setHorizontalAlignment(SwingConstants.CENTER);
        lblWagaY.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblWagaY.setBounds(494, 89, 58, 23);
        frame.getContentPane().add(lblWagaY);

        x_weight = new JLabel("x");
        x_weight.setHorizontalAlignment(SwingConstants.CENTER);
        x_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
        x_weight.setBounds(408, 114, 58, 23);
        frame.getContentPane().add(x_weight);

        y_weight = new JLabel("y");
        y_weight.setHorizontalAlignment(SwingConstants.CENTER);
        y_weight.setFont(new Font("Tahoma", Font.PLAIN, 16));
        y_weight.setBounds(494, 114, 58, 23);
        frame.getContentPane().add(y_weight);
        frame.setVisible(true);
    }

    class XYGraph extends JPanel {

        /**
         * Variables which contains info about currently added point.
         */
        Color clr;
        int x_var;
        int y_var;

        /**
         * Override to write custom point.
         */
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.setColor(clr);
            if (x_var != 0 && y_var != 0) {
                g.fillRect(x_var, y_var, size, size);
            }
            paintCross(g);
        }

        /**
         * Yet another smart function to draw a cross on the component.
         *
         * @param g
         */
        private void paintCross(Graphics g) {
            g.setColor(Color.BLACK);
            g.fillRect(180, 0, 1, 360);
            g.drawLine(176, 8, 180, 0);
            g.drawLine(184, 8, 180, 0);

            g.fillRect(0, 180, 360, 1);
            g.drawLine(352, 176, 360, 180);
            g.drawLine(352, 184, 360, 180);
            g.drawString("0", 183, 193);
        }

        /**
         * Writes a point at selected coordinates with selected colour.
         *
         * @param x coordinate X
         * @param y coordinate Y
         * @param clr selected colour
         */
        public void drawPoint(int x, int y, Color clr) {

            x += 180;
            y += 180;
            y = 360 - y;
            x_var = x;
            y_var = y;
            this.clr = clr;
            paintImmediately(x, y, size, size);
        }
    }
}
