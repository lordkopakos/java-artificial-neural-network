/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.network;

import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import psi.layer.Layer;
import psi.network.input.InputVectors;
import psi.network.input.VectorX;
import psi.neural.AdelineCell;
import psi.neural.LearningCell;

/**
 *
 * @author lordkopakos
 */
public class HopfieldNetwork {

    Layer layer;
    InputVectors inputVectors;
    int timeStep;

    private String fileName;

    public HopfieldNetwork() {
        timeStep = 1;
        initInputVectors(fileName);
        initLayer();
    }

    private void initInputVectors(String fileName) {
        try {
            inputVectors.initLearningSet(fileName);
        } catch (IOException ex) {
            Logger.getLogger(HopfieldNetwork.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initLayer() {
        int numberOfNeurals = inputVectors.getVectorLength();
        layer = new Layer(numberOfNeurals);
    }

    public boolean learn() {
        learnUsingAllVectors();
        return true;
    }

    private void learnUsingAllVectors() {
        inputVectors.getLearningSet().stream().forEach((vectorX) -> {
            
            int numberOfNeurals = layer.getNumberOfNeurals();

        for (int neuralIndex = 0; neuralIndex < numberOfNeurals; neuralIndex++) {
            recursiveLearnUsingOneVector(vectorX, neuralIndex);
        }
        });
    }

    private void recursiveLearnUsingOneVector(VectorX vectorX, int neuralIndex) {
        setLayerProperties(vectorX);
        recursiveCalculateNeuralOutput(neuralIndex);
        learnHebbMethod(vectorX,neuralIndex);
        setRecursiveinput(neuralIndex, layer.getNumberOfNeurals());
    }

    private void setLayerProperties(VectorX vectorX) {
        int numberOfNeurals = layer.getNumberOfNeurals();
        for (int neuralIndex = 0; neuralIndex < numberOfNeurals; neuralIndex++) {
            addNeuralInput(numberOfNeurals);
            setInputAsOutput(vectorX, neuralIndex);
            setActivationFuction(neuralIndex);
            setNeuralWeights(neuralIndex);
        }
    }

    private void setInputAsOutput(VectorX vectorX, int neuralIndex) {
        layer.setFinalizeData(neuralIndex, vectorX.getCoordinates(neuralIndex));

    }

    private void setActivationFuction(int neuralIndex) {
        layer.neurals.get(neuralIndex).setState(AdelineCell.Function.BIPOLAR_THRESHOLD_FUNCTION);
    }
    
    private void addNeuralInput(int numberOfInputs){
        layer.addInput(numberOfInputs);
    }

    private void setNeuralWeights(int neuralIndex) {
        layer.neurals.get(neuralIndex).setWeight(0);
    }

    private void recursiveCalculateNeuralOutput(int neuralIndex) {

            layer.setFinalizeData(neuralIndex, layer.neurals.get(neuralIndex).getMembranePotential());
        
    }
    
    private void learnHebbMethod(VectorX vectorX,int neuralIndex){
        layer.neurals.get(neuralIndex).HebbRule(vectorX);
    }
    private void setRecursiveinput(int neuralIndex, int numberOfNeurals){
            for(int finalizeDataIndex=0; finalizeDataIndex<numberOfNeurals;finalizeDataIndex++){
                if(neuralIndex!=finalizeDataIndex){
                    layer.neurals.get(neuralIndex).setInputData(finalizeDataIndex, layer.finalizeData.getCoordinates(finalizeDataIndex));
                }
            }
        
    }

}
