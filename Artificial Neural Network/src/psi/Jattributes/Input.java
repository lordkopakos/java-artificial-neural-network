/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.Jattributes;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.border.Border;
import psi.Jattributes.text.InputComponent;

/**
 *
 * @author lordkopakos
 */
public class Input {

    public ArrayList<JLabel> lbXs;
    public ArrayList<InputComponent> txtXs;
    public ArrayList<JLabel> lbArrows;
    public ArrayList<JLabel> lbWeights;
    public ArrayList<InputComponent> txtWeights;
    private int arrowWidth;

    public Input(int size) {
        lbXs = new ArrayList<>(size);
        txtXs = new ArrayList<>(size);
        lbArrows=new ArrayList<>(size);
        lbWeights=new ArrayList<>(size);
        txtWeights=new ArrayList<>(size);
        arrowWidth=143;
    }

    public void addNewXLabel(ArrayList<JLabel> array, String name, int width, int height, int locX, int locY, Color c, boolean visible) {
        JLabel l = new JLabel(name);
        l.setSize(width, height);
        l.setLocation(locX, locY);
        l.setBackground(c);
        l.setForeground(c);
        l.setVisible(visible);
        l.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
        array.add(l);
    }

    public void addNewXText(ArrayList<InputComponent> array, int width, int height, int locX, int locY, Color c, boolean visible) {
        InputComponent textField = new InputComponent();
        Border border
                = BorderFactory.createLineBorder(c, 3);
        textField.setSize(width, height);
        textField.setLocation(locX + width, locY);
        textField.setOpaque(false);
        textField.setBorder(border);
        textField.setBackground(c);
        textField.setForeground(c);
        textField.setVisible(visible);
        textField.setToolTipText("Vector x");
        textField.setFont(new Font("Gill Sans Ultra Bold", Font.PLAIN, 15));
        array.add(textField);
    }

    public void addArrowsAndWeights(int width, int height, int locX, int locY) {
            ImageIcon imgArrow = new ImageIcon("images/menu/main/arrows/blueArrow.png");
            JLabel lbArrow = new JLabel(imgArrow);
            lbArrow.setSize(143, 30);
            lbArrow.setLocation(locX, locY+10);
            lbArrows.add(lbArrow);
     
    }
    
    public void setArrowsAndWeightVisible(){
        for(JLabel arrow : lbArrows){
            arrow.setVisible(false);
        }
    }

    public void addVector(int index, int width, int height, int locX, int locY) {
        this.addNewXLabel(lbXs, "X" + index + ":", width, height, locX, locY, Color.BLACK, false);
        this.addNewXText(txtXs,width, height, locX, locY,Color.BLACK,false);
        this.addArrowsAndWeights(width, height, locX+width*2, locY);
        this.setArrowsAndWeightVisible();
        this.addNewXLabel(lbWeights, "W" + index + ":", width, height, locX+width*2+arrowWidth, locY, Color.BLACK, false);
        this.addNewXText(txtWeights,width, height, locX+2*width+arrowWidth, locY,Color.BLACK,false);
    }
    
    public int getArrowWidth(){
        return arrowWidth;
    }
    
    public void setArrowWidth(int arrowWidth){
        this.arrowWidth=arrowWidth;
    }
}
