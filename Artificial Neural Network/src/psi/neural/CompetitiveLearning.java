/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package psi.neural;

import psi.network.input.VectorX;

/**
 *
 * @author lordkopakos
 */
public interface CompetitiveLearning {
    public double kohonenRule(VectorX vectorX, double gX);
}
