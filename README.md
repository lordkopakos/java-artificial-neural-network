Artificial Neural Network

Wybór interesującej nas sieci neuronowej. Do wyboru pojedyńczy perceptron i lub sieć wielowarstwowa.

![Untitled3.jpg](https://bitbucket.org/repo/XX5Bo4x/images/3272573613-Untitled3.jpg)

Tworzenie Perceptronu.

![Untitled1.jpg](https://bitbucket.org/repo/XX5Bo4x/images/1464781094-Untitled1.jpg)


1. Panel opcji wczytywania plików. Storzone zostały nowe rozszerzenia .ts, .vs i .test reprezentujące kolejno: dane uczące, walidujące i testujące. Program wyszukuje tylko pliki z podanymi rozszerzeniami ignorując inne, dzięki czemu w wyborze plików nie pojawiają się „pliki śmieci”.

2. ComboBox odpowiadający za wybór plików z danymi uczącymi, czyli plików z rozszerzeniem .ts.

3. ComboBox odpowiadający za wybór plików z danymi walidującymi, czyli plików z rozszerzeniem .vs.

4. ComboBox odpowiadający za wybór plików z danymi testującymi, czyli plików z rozszerzeniem .test.

5. Wizualizacja neuronu, po naciśnięciu na niego pojawia się panel ustawienia przedziału wag, a także możliwość ustawienia biasu i wartości liczby β używaniej do obliczenia wartości bipolarnej i unipolarnej funkcji sigmoidalnej.

6. Wybór funkcji aktywacji neuronu. Wybrać możnaz pośrud następujących funkcji aktywacji: bipolarna funkcja progowa, unipolarna funkcja progowa, bipolarna funkcja 

7. Graf prezentujący wykes funkcji celu błędu MSE

8. Graf prezentujący wykes funkcji celu błędu MAPE 

Wybór przedziału losowanych wag.

![Untitled.jpg](https://bitbucket.org/repo/XX5Bo4x/images/2408770165-Untitled.jpg)

Przeprowadzenie epoki uczenia.

![Untitled2.jpg](https://bitbucket.org/repo/XX5Bo4x/images/1422428829-Untitled2.jpg)

Dodawanie kolejnych warstw do sieci z określoną liczbą neuronów. Całość oparta na ArrayListach które dają większą swobode w tworzeniu struktur takich jak sieci wielo warstwowe.

![Untitled4.jpg](https://bitbucket.org/repo/XX5Bo4x/images/65735000-Untitled4.jpg)